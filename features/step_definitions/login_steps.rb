Dado("que estou na tela de login UP") do
  visit 'https://hml.upsaudecorporativa.com.br/#/'
end

Quando("realizo login com o usuário {string} e {string}") do |usuario, senha|
  find('input[name=email]').set usuario
  find('input[name=password]').set senha
  click_button 'Enviar'
end

Então("devo ver a mensagem {string}") do |perfil_admin|
  sleep 5
  page.has_css?('div', text: 'Admin Valid')
end

