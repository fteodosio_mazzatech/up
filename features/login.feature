# language: pt

  Funcionalidade: Validar Login no UP
  Validar funcionalidades de Login no UP

  Contexto: Página de Login
    Dado que estou na tela de login UP

    
    Cenário: Login - Autenticar com sucesso
        
      Quando realizo login com o usuário "admin@valid.com.br" e "123456"
      Então devo ver a mensagem "perfil_admin"
  

  Esquema do Cenário: Login - Tentativa de login invalidas
      Quando realizo login com o usuário "<usuario>" e "<senha>"
      Então devo ver a mensagem "<alerta>"
      
      Exemplos:
      |usuario|senha|alerta|
      |admin@valid.com|12345678|Opa, o e-mail ou a senha estão errados.|
      |admin@valid.net|123456|Opa, o e-mail ou a senha estão errados.|
      |admin@valid.net|12345678|Opa, o e-mail ou a senha estão errados.|


    



